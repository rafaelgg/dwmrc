#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <Xutil.h>
#include <Xatom.h>
#include <fcntl.h>

#define BATTERY "BAT0"
#define BATT_CHARGE_FULL "/sys/class/power_supply/"BATTERY"/charge_full"
#define BATT_CHARGE_NOW "/sys/class/power_supply/"BATTERY"/charge_now"
#define BATT_ENERGY_FULL "/sys/class/power_supply/"BATTERY"/energy_full"
#define BATT_ENERGY_NOW "/sys/class/power_supply/"BATTERY"/energy_now"
#define DATE_FORMAT "%H:%M (%Z) - %a %d.%m.%Y"
#define BUFFER_SIZE 4096
#define STATUS_SIZE 256
#define UPDATE_TIMEOUT 10
#define MESSAGE_TIMEOUT 3
#define FIFOBASE "/tmp/dwmrc"

enum {
  BATT_CHARGE = 0,
  BATT_POWER  = 1,
};

static char *buf;
static Display *dpy;
static Window root;
static XTextProperty name;
static int fifofd;
static int batt_i = BATT_CHARGE;
static char fifopath[PATH_MAX];

typedef struct {
  const char *full_path;
  const char *now_path;
} Battery;

static const Battery batt_file[] = {
  { BATT_CHARGE_FULL, BATT_CHARGE_NOW },
  { BATT_ENERGY_FULL, BATT_ENERGY_NOW },
};

void update_status(char *c) {
	name.value=(unsigned char*)c;
	name.nitems=strlen(c);
	XSetWMName(dpy, root, &name);
	XSync(dpy, False);
}

void cleanup(void) {
	free(buf);
}

void die(int unused) {
	close(fifofd);
	unlink(fifopath);
	update_status("dwmrc died!");
	exit(EXIT_FAILURE);
}

int read_batt(const char *batt_path) {
	FILE *f;
	int batt_total = -1;

	f = fopen(batt_path, "r");
	if(!f) return -1;
	fread(buf, 1, BUFFER_SIZE, f);
	batt_total = atoi(buf);
	fclose(f);
	return batt_total;
}

int main(int argc, char **argv) {
	time_t cur_t;
	struct tm *cur_lt;
	char *status;
	int batt_full, batt_cur;
	int n;
	struct timeval tv;
	fd_set fds;

	fifopath[0] = '\0';
	snprintf(fifopath, sizeof(fifopath), "%s.%s.%d", FIFOBASE, getenv("USER"), getpid());

	if( mkfifo(fifopath, (S_IRUSR | S_IWUSR)) != 0 ||
		(fifofd = open(fifopath, O_RDWR | O_NONBLOCK)) == -1 ||
		!(dpy = XOpenDisplay(NULL)) ||
		!(buf = calloc(1, 1+sizeof(char)*BUFFER_SIZE)) ||
		!(status = calloc(1, 1+sizeof(char)*STATUS_SIZE)) ) die(0);

	root = RootWindow(dpy, DefaultScreen(dpy));
	name.encoding=XA_STRING;
	name.format=8;

	signal(SIGINT, die);
	signal(SIGILL, die);
	signal(SIGTERM, die);

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	while(1) {
		FD_ZERO(&fds);
		FD_SET(fifofd, &fds);
		if( select(FD_SETSIZE, &fds, NULL, NULL, &tv) > 0 &&
		    FD_ISSET(fifofd, &fds) &&
		     (n = read(fifofd,buf,BUFFER_SIZE)) > 0 ){
			buf[n] = '\0';
			for(n=0; n<strlen(buf); n++) {
				if(buf[n] == '\n') buf[n] = ' ';
			}
			strncpy(status, buf, STATUS_SIZE);
			status[STATUS_SIZE-1] = '\0';
			tv.tv_sec = MESSAGE_TIMEOUT;
			tv.tv_usec = 0;
		} else {
			batt_full=read_batt(batt_file[batt_i].full_path);
			batt_cur =read_batt(batt_file[batt_i].now_path);

			if(batt_full <= 0 || batt_cur < 0) {
				batt_i ^= 1;
				batt_full=read_batt(batt_file[batt_i].full_path);
				batt_cur =read_batt(batt_file[batt_i].now_path);
			}

			if(batt_full > 0 && batt_cur >= 0) n=snprintf(status, STATUS_SIZE, "%3d%% -- ", (100*batt_cur/batt_full));
			else n=snprintf(status, STATUS_SIZE, " NAN -- ");

			cur_t = time(NULL);
			cur_lt = localtime(&cur_t);
			strftime(&status[n], STATUS_SIZE-n, DATE_FORMAT, cur_lt);
			tv.tv_sec = UPDATE_TIMEOUT;
			tv.tv_usec = 0;
		}
		update_status(status);
	}

	return EXIT_SUCCESS;
}

